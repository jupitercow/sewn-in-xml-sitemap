=== Plugin Name ===
Contributors: jcow
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=jacobsnyder%40gmail%2ecom&lc=US&item_name=Jacob%20Snyder&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_SM%2egif%3aNonHosted
Tags: gravity forms, update posts, frontend, front end
Requires at least: 3.6.1
Tested up to: 3.9.1
Stable tag: 1.2.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Simple way to automatically generate XML Sitemaps when a page or post is saved. Very simple, no cruft or extra features you won't use.

== Description ==

There are two main customizations available.

*	Choose which post types are added (posts and pages by default)
*	When ACF is installed, adds a metabox to all included post types to remove single posts

= Add or remove post types =

By default only pages and posts are added, but you can remove either of those and/or add more using this filter.

`/**
 * Customize what post types are added to the XML sitemap
 *
 * @param	array	$post_types	List of post types to be added to the XML Sitemap
 * @return	array	$post_types	Modified list of post types
 */
add_filter( 'sewn_xml_sitemap/post_types', 'custom_sitemap_post_types' );
function custom_sitemap_post_types( $post_types )
{
	$post_types[] = "news";
	return $post_types;
}`

= Remove a specific post from the sitemap =

A checkbox is added to each post type that is included in the sitemap. Checking it will remove that specific item from the sitemap.

This checkbox also removes posts from wp_list_pages, you can turn that off using this filter:

```php
add_filter( 'sewn_xml_sitemap/wp_list_pages', '__return_false' )
```

= Compatibility =

This works with the Sewn In Simple SEO plugin. When installed, the XML sitemap checkbox integrates with the SEO fields and this plugin will use the SEO post types. The goal is to keep things very simple and integrated.

== Installation ==

*   Copy the folder into your plugins folder
*   Activate the plugin via the Plugins admin page

== Frequently Asked Questions ==

= No questions yet. =

== Screenshots ==

1. The checkbox to remove posts in the backend.

== Changelog ==

## 1.0.3 - 2014-08-03

- Added to the repo

== Upgrade Notice ==

= 1.0.3 =
This is the first version in the Wordpress repository.
